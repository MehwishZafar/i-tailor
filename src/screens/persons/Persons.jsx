import { View, Text } from "react-native";
import React from "react";
import { personStyles } from "./personStyles";

export default function Persons() {
  return (
    <View style={personStyles.container}>
      <Text style={personStyles.label}>Persons</Text>
    </View>
  );
}
