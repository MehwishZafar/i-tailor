import {
  Text,
  StyleSheet,
  View,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image,
} from "react-native";
import React, { useState } from "react";
import Ionicons from "@expo/vector-icons/Ionicons";
import { colors } from "../utils/theme";

export default function Register() {
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [userGender, setUserGender] = useState();
  const [showPass, setShowPass] = useState(true);

  function onRegisterPress() {
    console.log({
      firstName,
      lastName,
      email,
      password,
      userGender,
    });
  }

  function onEyePress() {
    if (showPass === true) {
      setShowPass(false);
    } else if (showPass === false) {
      setShowPass(true);
    }
  }

  return (
    <ScrollView>
      <View style={styles.formCon}>
        <Image
          style={{ width: 100, height: 100 }}
          source={require("../../assets/logo.png")}
        />
        <Image
          style={{ width: 100, height: 100 }}
          source={{
            uri: "https://cdn.pixabay.com/photo/2016/06/23/18/55/apple-1475977_1280.png",
          }}
        />
        <TextInput
          placeholder={"First Name"}
          style={styles.inputCon}
          onChangeText={setFirstName}
        />

        <TextInput
          placeholder={"Last Name"}
          style={styles.inputCon}
          onChangeText={setLastName}
        />

        <TextInput
          placeholder={"Email"}
          style={styles.inputCon}
          onChangeText={setEmail}
        />

        <View style={styles.passwordCon}>
          <TextInput
            placeholder={"Password"}
            onChangeText={setPassword}
            secureTextEntry={showPass}
          />
          <Ionicons
            name={showPass === true ? "eye-off" : "eye"}
            color={showPass === true ? colors.accent : "red"}
            size={18}
            onPress={onEyePress}
          />
        </View>

        <TextInput
          placeholder={"Gender"}
          style={styles.inputCon}
          onChangeText={setUserGender}
        />

        <TouchableOpacity style={styles.btn} onPress={onRegisterPress}>
          <Text style={styles.btnText}>Register</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  formCon: {
    width: "100%",
    marginTop: 200,
    padding: 10,
  },

  inputCon: {
    borderWidth: 1,
    borderColor: colors.primary,
    padding: 10,
    borderRadius: 10,
    marginVertical: 5,
  },

  passwordCon: {
    borderWidth: 1,
    borderColor: colors.primary,
    padding: 10,
    borderRadius: 10,
    marginVertical: 5,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },

  btn: {
    padding: 10,
    borderRadius: 5,
    width: "60%",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    backgroundColor: colors.primary,
  },

  btnText: {
    color: colors.white,
  },
});
