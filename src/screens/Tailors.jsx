import { StyleSheet, Text, View, FlatList } from "react-native";
import React from "react";

const Tailors = () => {
  const myAges = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 100, 200, 414, 890, 990, 1100, 440, 0.5,
  ];

  const __renderItem = ({ item }) => (
    <View style={styles.itemCon}>
      <Text>{item}</Text>
    </View>
  );

  return (
    <View style={styles.con}>
      <FlatList data={myAges} renderItem={__renderItem} horizontal={true} />
    </View>
  );
};

export default Tailors;

const styles = StyleSheet.create({
  con: {
    flex: 1,
    marginTop: 20,
  },

  itemCon: {
    width: 100,
    height: 100,
    padding: 10,
    backgroundColor: "orange",
    marginHorizontal: 10,
  },
});
